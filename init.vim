let g:material_theme_style = 'dark'

if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

"For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
if (has("termguicolors"))
  set termguicolors
endif

" Plug

call plug#begin('~/.local/share/nvim/plugged')

Plug 'kaicataldo/material.vim'
Plug 'tomasiser/vim-code-dark'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'https://github.com/scrooloose/nerdtree'

call plug#end()

" Colors

set background=dark
set nu
syntax on
syntax enable
colorscheme material

" airline
let g:material_terminal_italics = 1
set laststatus=2 

" Mappings
nnoremap <c-j> <c-w><c-j>
nnoremap <c-k> <c-w><c-k>
nnoremap <c-l> <c-w><c-l>
nnoremap <c-h> <c-w><c-h>
nnoremap <C-Tab> :bn<CR>
nnoremap <leader>bd :bd<CR>
map <C-n> :NERDTreeToggle<CR>
:tnoremap <Esc> <C-\><C-n>
nnoremap <leader>i mzgg=G`z<CR>
nmap <Leader>s :%s//g<Left><Left>
nmap <c-leftmouse> gx

" Move line up and down 
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Settings
set incsearch
set hlsearch 
set cursorline 
set noruler
set tabstop=4           " Render TABs using this many spaces.
set shiftwidth=4        " Indentation amount for < and > commands.
set clipboard+=unnamedplus
